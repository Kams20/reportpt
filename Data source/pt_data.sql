-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.5.5-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for pt_report
CREATE DATABASE IF NOT EXISTS `pt_report` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `pt_report`;

-- Dumping structure for table pt_report.performance_testing
CREATE TABLE IF NOT EXISTS `performance_testing` (
  `RUN_TYPE` mediumtext DEFAULT NULL,
  `CHANNEL` mediumtext DEFAULT NULL,
  `APPLICATION` mediumtext DEFAULT NULL,
  `Environment` mediumtext DEFAULT NULL,
  `LOB` mediumtext DEFAULT NULL,
  `TRANSACTIONNAME` mediumtext DEFAULT NULL,
  `Release` varchar(50) DEFAULT NULL,
  `EXECUTEDON` mediumtext DEFAULT NULL,
  `SLA` int(11) DEFAULT NULL,
  `ResponseTimeInsecs` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table pt_report.performance_testing: ~106 rows (approximately)
/*!40000 ALTER TABLE `performance_testing` DISABLE KEYS */;
REPLACE INTO `performance_testing` (`RUN_TYPE`, `CHANNEL`, `APPLICATION`, `Environment`, `LOB`, `TRANSACTIONNAME`, `Release`, `EXECUTEDON`, `SLA`, `ResponseTimeInsecs`) VALUES
	('Smoke test', 'Agent', 'PC', 'Accp01', 'PL', 'Quote creation', '3.1', '2/6/2020', 6, 8),
	('Smoke test', 'Agent', 'PC', 'Accp01', 'CL', 'Quote creation', '3.1', '2/6/2020', 4, 7),
	('Smoke test', 'Agent', 'PC', 'Accp01', 'PL', 'Quote creation', '3.2', '2/6/2020', 4, 7.68),
	('Smoke test', 'Agent', 'PC', 'Accp01', 'CL', 'Quote creation', '3.2', '2/6/2020', 4, 11),
	('Smoke test', 'Broker', 'PC', 'Accp01', 'PL', 'Quote creation', '3.2', '2/6/2020', 4, 7.68),
	('Smoke test', 'Broker', 'PC', 'Accp01', 'CL', 'Quote creation', '3.2', '2/6/2020', 4, 11),
	('Smoke test', 'Agent', 'PC', 'Accp01', 'PL', 'Quote creation', '3.2', '2/10/2020', 4, 27.8),
	('Smoke test', 'Agent', 'PC', 'Accp01', 'CL', 'Quote creation', '3.2', '2/10/2020', 4, 8),
	('Smoke test', 'Broker', 'PC', 'Accp01', 'PL', 'Quote creation', '3.2', '2/10/2020', 4, 27.8),
	('Smoke test', 'Broker', 'PC', 'Accp01', 'CL', 'Quote creation', '3.2', '2/10/2020', 4, 8),
	('Smoke test', 'Agent', 'PC', 'Accp01', 'PL', 'Quote creation', '3.2', '2/13/2020', 4, 10.44),
	('Smoke test', 'Agent', 'PC', 'Accp01', 'CL', 'Quote creation', '3.2', '2/13/2020', 4, 11),
	('Smoke test', 'Broker', 'PC', 'Accp01', 'PL', 'Quote creation', '3.2', '2/13/2020', 4, 10.83),
	('Smoke test', 'Broker', 'PC', 'Accp01', 'CL', 'Quote creation', '3.2', '2/13/2020', 4, 11),
	('Smoke test', 'Agent', 'PC', 'Accp01', 'PL', 'Quote creation', '3.2', '2/14/2020', 4, 7.62),
	('Smoke test', 'Agent', 'PC', 'Accp01', 'CL', 'Quote creation', '3.2', '2/14/2020', 4, 9),
	('Smoke test', 'Broker', 'PC', 'Accp01', 'PL', 'Quote creation', '3.2', '2/14/2020', 4, 7.32),
	('Smoke test', 'Broker', 'PC', 'Accp01', 'CL', 'Quote creation', '3.2', '2/14/2020', 4, 8),
	('Smoke test', 'Agent', 'PC', 'Accp01', 'PL', 'Quote creation', '3.2', '2/18/2020', 4, 9.22),
	('Smoke test', 'Agent', 'PC', 'Accp01', 'CL', 'Quote creation', '3.2', '2/18/2020', 4, 11.2),
	('Smoke test', 'Broker', 'PC', 'Accp01', 'PL', 'Quote creation', '3.2', '2/18/2020', 4, 9),
	('Smoke test', 'Broker', 'PC', 'Accp01', 'CL', 'Quote creation', '3.2', '2/18/2020', 4, 9.15),
	('Smoke test', 'Agent', 'PC', 'Accp01', 'PL', 'Quote creation', '3.2', '2/20/2020', 4, 10.22),
	('Smoke test', 'Agent', 'PC', 'Accp01', 'CL', 'Quote creation', '3.2', '2/20/2020', 4, 11.9),
	('Smoke test', 'Broker', 'PC', 'Accp01', 'PL', 'Quote creation', '3.2', '2/20/2020', 4, 10.67),
	('Smoke test', 'Broker', 'PC', 'Accp01', 'CL', 'Quote creation', '3.2', '2/20/2020', 4, 9.73),
	('Smoke test', 'Agent', 'PC', 'Accp01', 'PL', 'Quote creation', '3.2', '2/22/2020', 4, 10.24),
	('Smoke test', 'Agent', 'PC', 'Accp01', 'CL', 'Quote creation', '3.2', '2/22/2020', 4, 10),
	('Smoke test', 'Broker', 'PC', 'Accp01', 'PL', 'Quote creation', '3.2', '2/22/2020', 4, 12.1),
	('Smoke test', 'Broker', 'PC', 'Accp01', 'CL', 'Quote creation', '3.2', '2/22/2020', 4, 13.8),
	('Smoke test', 'Agent', 'PC', 'Accp01', 'PL', 'Quote creation', '3.2', '2/24/2020', 4, 8.97),
	('Smoke test', 'Agent', 'PC', 'Accp01', 'CL', 'Quote creation', '3.2', '2/24/2020', 4, 8.64),
	('Smoke test', 'Broker', 'PC', 'Accp01', 'PL', 'Quote creation', '3.2', '2/24/2020', 4, 10.16),
	('Smoke test', 'Broker', 'PC', 'Accp01', 'CL', 'Quote creation', '3.2', '2/24/2020', 4, 8.61),
	('Smoke test', 'Agent', 'PC', 'Accp01', 'PL', 'Quote creation', '3.2', '2/25/2020', 4, 9.68),
	('Smoke test', 'Agent', 'PC', 'Accp01', 'CL', 'Quote creation', '3.2', '2/25/2020', 4, 9.4),
	('Smoke test', 'Broker', 'PC', 'Accp01', 'PL', 'Quote creation', '3.2', '2/25/2020', 4, 6.07),
	('Smoke test', 'Broker', 'PC', 'Accp01', 'CL', 'Quote creation', '3.2', '2/25/2020', 4, 8.59),
	('Smoke test', 'Agent', 'PC', 'Accp01', 'PL', 'Quote creation', '3.2', '2/26/2020', 4, 10.4),
	('Smoke test', 'Agent', 'PC', 'Accp01', 'CL', 'Quote creation', '3.2', '2/26/2020', 4, 7.03),
	('Smoke test', 'Broker', 'PC', 'Accp01', 'PL', 'Quote creation', '3.2', '2/26/2020', 4, 6.4),
	('Smoke test', 'Broker', 'PC', 'Accp01', 'CL', 'Quote creation', '3.2', '2/26/2020', 4, 7.94),
	('Smoke test', 'Agent', 'PC', 'Accp01', 'PL', 'Quote creation', '3.2', '2/27/2020', 5, 8.63),
	('Smoke test', 'Agent', 'PC', 'Accp01', 'CL', 'Quote creation', '3.2', '2/27/2020', 5, 8.76),
	('Smoke test', 'Broker', 'PC', 'Accp01', 'PL', 'Quote creation', '3.2', '2/27/2020', 5, 12.7),
	('Smoke test', 'Broker', 'PC', 'Accp01', 'CL', 'Quote creation', '3.2', '2/27/2020', 5, 6.21),
	('Smoke test', 'Agent', 'PC', 'Accp01', 'PL', 'Quote creation', '3.2', '2/28/2020', 5, 30.1),
	('Smoke test', 'Agent', 'PC', 'Accp01', 'CL', 'Quote creation', '3.2', '2/28/2020', 5, 7.35),
	('Smoke test', 'Broker', 'PC', 'Accp01', 'PL', 'Quote creation', '3.2', '2/28/2020', 5, 30.1),
	('Smoke test', 'Broker', 'PC', 'Accp01', 'CL', 'Quote creation', '3.2', '2/28/2020', 5, 5.77),
	('Smoke test', 'Agent', 'PC', 'Accp01', 'PL', 'Quote creation', '3.2', '3/2/2020', 5, 9.27),
	('Smoke test', 'Agent', 'PC', 'Accp01', 'CL', 'Quote creation', '3.2', '3/2/2020', 5, 8.87),
	('Smoke test', 'Broker', 'PC', 'Accp01', 'PL', 'Quote creation', '3.2', '3/2/2020', 5, 7.08),
	('Smoke test', 'Broker', 'PC', 'Accp01', 'CL', 'Quote creation', '3.2', '3/2/2020', 5, 6.53),
	('Smoke test', 'Agent', 'PC', 'Accp01', 'PL', 'Quote creation', '3.2', '3/3/2020', 5, 9.47),
	('Smoke test', 'Agent', 'PC', 'Accp01', 'CL', 'Quote creation', '3.2', '3/3/2020', 5, 9.11),
	('Smoke test', 'Broker', 'PC', 'Accp01', 'PL', 'Quote creation', '3.2', '3/3/2020', 5, 6.33),
	('Smoke test', 'Broker', 'PC', 'Accp01', 'CL', 'Quote creation', '3.2', '3/3/2020', 5, 9.81),
	('Smoke test', 'Agent', 'PC', 'Accp01', 'PL', 'Quote creation', '3.2', '3/4/2020', 5, 9.18),
	('Smoke test', 'Agent', 'PC', 'Accp01', 'CL', 'Quote creation', '3.2', '3/4/2020', 5, 7.56),
	('Smoke test', 'Broker', 'PC', 'Accp01', 'PL', 'Quote creation', '3.2', '3/4/2020', 5, 5.92),
	('Smoke test', 'Broker', 'PC', 'Accp01', 'CL', 'Quote creation', '3.2', '3/4/2020', 5, 7.76),
	('Smoke test', 'Agent', 'PC', 'Accp01', 'PL', 'Quote creation', '3.2', '3/6/2020', 5, 22.3),
	('Smoke test', 'Agent', 'PC', 'Accp01', 'CL', 'Quote creation', '3.2', '3/6/2020', 5, 9.94),
	('Smoke test', 'Broker', 'PC', 'Accp01', 'PL', 'Quote creation', '3.2', '3/6/2020', 5, 9.66),
	('Smoke test', 'Broker', 'PC', 'Accp01', 'CL', 'Quote creation', '3.2', '3/6/2020', 5, 9.96),
	('Smoke test', 'Agent', 'PC', 'Accp01', 'PL', 'Quote creation', '3.2', '3/9/2020', 4, 9.36),
	('Smoke test', 'Agent', 'PC', 'Accp01', 'CL', 'Quote creation', '3.2', '3/9/2020', 4, 9.36),
	('Smoke test', 'Broker', 'PC', 'Accp01', 'PL', 'Quote creation', '3.2', '3/9/2020', 4, 6.54),
	('Smoke test', 'Broker', 'PC', 'Accp01', 'CL', 'Quote creation', '3.2', '3/9/2020', 4, 9.96),
	('Smoke test', 'Agent', 'PC', 'Accp01', 'PL', 'Quote creation', '3.2', '3/9/2020', 4, 9.36),
	('Smoke test', 'Agent', 'PC', 'Accp01', 'CL', 'Quote creation', '3.2', '3/9/2020', 4, 9.36),
	('Smoke test', 'Broker', 'PC', 'Accp01', 'PL', 'Quote creation', '3.2', '3/9/2020', 4, 6.54),
	('Smoke test', 'Broker', 'PC', 'Accp01', 'CL', 'Quote creation', '3.2', '3/9/2020', 4, 9.96),
	('Smoke test', 'Agent', 'PC', 'Accp01', 'PL', 'Quote creation', '3.2', '3/17/2020', 4, 9),
	('Smoke test', 'Agent', 'PC', 'Accp01', 'CL', 'Quote creation', '3.2', '3/17/2020', 4, 9.09),
	('Smoke test', 'Broker', 'PC', 'Accp01', 'PL', 'Quote creation', '3.2', '3/17/2020', 4, 6.3),
	('Smoke test', 'Broker', 'PC', 'Accp01', 'CL', 'Quote creation', '3.2', '3/17/2020', 4, 7.91),
	('Smoke test', 'Agent', 'PC', 'Accp01', 'PL', 'Quote creation', '3.2', '3/23/2020', 4, 11),
	('Smoke test', 'Agent', 'PC', 'Accp01', 'CL', 'Quote creation', '3.2', '3/23/2020', 4, 12.5),
	('Smoke test', 'Broker', 'PC', 'Accp01', 'PL', 'Quote creation', '3.2', '3/23/2020', 4, 6.45),
	('Smoke test', 'Broker', 'PC', 'Accp01', 'CL', 'Quote creation', '3.2', '3/23/2020', 4, 9.42),
	('Smoke test', 'Agent', 'PC', 'Accp01', 'PL', 'Quote creation', '3.2', '3/24/2020', 4, 9.96),
	('Smoke test', 'Agent', 'PC', 'Accp01', 'CL', 'Quote creation', '3.2', '3/24/2020', 4, 9.15),
	('Smoke test', 'Broker', 'PC', 'Accp01', 'PL', 'Quote creation', '3.2', '3/24/2020', 4, 10),
	('Smoke test', 'Broker', 'PC', 'Accp01', 'CL', 'Quote creation', '3.2', '3/24/2020', 4, 9.16),
	('Smoke test', 'Agent', 'PC', 'Accp01', 'PL', 'Quote creation', '3.2', '3/26/2020', 4, 6.28),
	('Smoke test', 'Agent', 'PC', 'Accp01', 'CL', 'Quote creation', '3.2', '3/26/2020', 4, 11.9),
	('Smoke test', 'Broker', 'PC', 'Accp01', 'PL', 'Quote creation', '3.2', '3/26/2020', 4, 9.73),
	('Smoke test', 'Broker', 'PC', 'Accp01', 'CL', 'Quote creation', '3.2', '3/26/2020', 4, 8.19),
	('Smoke test', 'Agent', 'PC', 'Accp01', 'PL', 'Quote creation', '3.2', '4/7/2020', 4, 5.69),
	('Smoke test', 'Agent', 'PC', 'Accp01', 'CL', 'Quote creation', '3.2', '4/7/2020', 4, 9.2),
	('Smoke test', 'Broker', 'PC', 'Accp01', 'PL', 'Quote creation', '3.2', '4/7/2020', 4, 11.4),
	('Smoke test', 'Broker', 'PC', 'Accp01', 'CL', 'Quote creation', '3.2', '4/7/2020', 4, 6.52),
	('Smoke test', 'Agent', 'PC', 'Accp01', 'PL', 'Quote creation', '3.2', '4/9/2020', 4, 11.9),
	('Smoke test', 'Agent', 'PC', 'Accp01', 'CL', 'Quote creation', '3.2', '4/9/2020', 4, 8.97),
	('Smoke test', 'Broker', 'PC', 'Accp01', 'PL', 'Quote creation', '3.2', '4/9/2020', 4, 8.51),
	('Smoke test', 'Broker', 'PC', 'Accp01', 'CL', 'Quote creation', '3.2', '4/9/2020', 4, 9.64),
	('Smoke test', 'Agent', 'PC', 'Accp01', 'PL', 'Quote creation', '3.2', '4/21/2020', 4, 8.19),
	('Smoke test', 'Agent', 'PC', 'Accp01', 'CL', 'Quote creation', '3.2', '4/21/2020', 4, 8.97),
	('Smoke test', 'Broker', 'PC', 'Accp01', 'PL', 'Quote creation', '3.2', '4/21/2020', 4, 8.51),
	('Smoke test', 'Broker', 'PC', 'Accp01', 'CL', 'Quote creation', '3.2', '4/21/2020', 4, 9.64),
	('Smoke test', 'Agent', 'PC', 'Accp01', 'PL', 'Quote creation', '3.2', '4/23/2020', 4, 7.11),
	('Smoke test', 'Agent', 'PC', 'Accp01', 'CL', 'Quote creation', '3.2', '4/23/2020', 4, 10.94),
	('Smoke test', 'Broker', 'PC', 'Accp01', 'PL', 'Quote creation', '3.2', '4/23/2020', 4, 9.81),
	('Smoke test', 'Broker', 'PC', 'Accp01', 'CL', 'Quote creation', '3.2', '4/23/2020', 4, 9.36);
/*!40000 ALTER TABLE `performance_testing` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
