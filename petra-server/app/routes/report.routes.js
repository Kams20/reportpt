module.exports = (app) => {
  const reports = require("../controllers/report.controller");
  app.get("/reports", reports.findAll);
  app.get("/projects", reports.projectFilter);
  app.get("/release/:projectName", reports.ReleaseFilter);
  // app.get("/release", reports.ReleaseFilter);
};
