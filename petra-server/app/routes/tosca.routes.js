module.exports = (app) => {
    const reports = require("../controllers/tosca.controller");
    app.get("/toscareports", reports.findAll);
  };
  