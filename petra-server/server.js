let env = require('dotenv')
const express = require("express");
const bodyParser = require("body-parser");
cors = require("cors");
const app = express();

// app.use(bodyParser.json());
// app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json({limit: '50mb', extended: true}));        
app.use(bodyParser.text({limit: '50mb', extended: true}));        
app.use(bodyParser.urlencoded({limit:'50mb', extended: true }));    

app.use(cors());


app.get("/", (req, res) => {
  res.json({ message: "Welcome to PT-REPORT API Service Tool application." });
});

require("./app/routes/report.routes")(app);
require("./app/routes/tosca.routes")(app);

//initialize .env file
env.config()

const PORT = 9000;

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});
