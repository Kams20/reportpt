export const config = {
    appBaseName: process.env.REACT_APP_BASENAME,
    serviceUrl: "http://localhost:9000/",
    cLine : "commercialLine",
    allLob:"ALL",
    pLine: "personalLine",
    allReports:"reports",
    uniqueProjects:"projects",
    uniqueReleases:"release",
    uniqueTranscation:"transactionName",
    nodatadisplay:"No Data to Display",
    release:"Release",
    runType:"RUN_TYPE",
    channel: "CHANNEL",
    executedon:"EXECUTEDON",
    toscaReports:"toscareports"
};