import React, { useEffect, useState } from "react";
import PieChart from "./pieChart";
import { connect } from "react-redux";
import "antd/dist/antd.css";
import { Layout, Select, Divider } from "antd";
import { fetchToscaReport } from '../redux/actions/toscaAction';
import Logo from "../assets/pvlogo_new.png";
import { PieChartFunctionFirstLevel, TableDataFilter } from "../chartFunctions/toscaReportFunction";
import ToscaReportTable from './Table';
import { CSVLink, CSVDownload } from "react-csv";
import { TableViewFunction } from "../redux/actions/toscaAction";
import { Button, message } from 'antd';
let initialLoad = true;

const { Header, Content } = Layout;
const { Option } = Select;

const ToscaReport = (props: any, state: any) => {

  useEffect(() => {
    //Runs only on the first render
    if (props.toscaReports.length === 0) {
      props.fetchToscaReport();
    }
    if (props.toscaReports.length !== 0 && initialLoad) {
      console.log("Inside initial load")
      initialLoad = false;
      SetType(props.uniqueType[0]);
      let DataFilter = props.toscaReports.filter((data: any) => { return (data.Type === props.uniqueType[0]) });
      let distnictDomain: any = [...new Set(DataFilter.map((x: any) => x.Domain))];
      SetfilteredDomain(distnictDomain);
  
    }

  }, [state,props.uniqueType]);

  const [selectedType, SetType] = useState('');
  const [selectedDomain, SetDomain] = useState('');
  const [selectedApplication, SetApplication] = useState('');
  const [selectedDate, setDate] = useState('');
  const [filteredDomain, SetfilteredDomain] = useState([])
  const [filteredApplication, SetfilteredApplication] = useState([])
  const [filteredDate, SetfilteredDate] = useState([])


  const TypeSelect = (params: any) => {
    SetType(params);
    SetDomain('');
    SetApplication('');
    setDate('');
    let DataFilter = props.toscaReports.filter((data: any) => { return (data.Type === params) });
    console.log(1, DataFilter)
    let distnictDomain: any = [...new Set(DataFilter.map((x: any) => x.Domain))];
    console.log(2, distnictDomain)
    SetfilteredDomain(distnictDomain);
  }

  const DomainSelect = (params: any) => {
    SetDomain(params);
    SetApplication('');
    setDate('');
    let DataFilter = props.toscaReports.filter((data: any) => { return (data.Domain === params && data.Type === selectedType) });
    let distnictApplication: any = [...new Set(DataFilter.map((x: any) => x.Application))];
    SetfilteredApplication(distnictApplication);
  }

  const ApplicationSelect = (params: any) => {
    SetApplication(params);
    setDate('');
    let DataFilter = props.toscaReports.filter((data: any) => { return (data.Domain === selectedDomain && data.Type === selectedType && data.Application === params) });
    let distnictDate: any = [...new Set(DataFilter.map((x: any) => x.Date))];
    console.log(distnictDate)
    SetfilteredDate(distnictDate);
  };

  const DateSelect = (params: any) => {
    setDate(params);

  };

  return (
    <>
      {props.toscaReports.length !== 0 ? (
        <Layout style={{ minHeight: "100vh" }}>
          <Header className="header"
            style={{ padding: 0, position: 'fixed', zIndex: 1, width: '100%' }}
          >
            <div className="logo">
              <img
                src={Logo}
                alt="PV"
                className="left-sec pv_logo"
              />
            </div>
            <div
              style={{ float: "right", marginLeft: "auto" }}
            >
              <Select
                // mode="multiple"
                className="header-btn"
                showSearch
                // style={{ width: `${(8 * selectedApplication.length) + 500}px`, height: "100px", backgroundColor: "white" }}
                value={selectedDate === '' ? (undefined) : (selectedDate)}
                onChange={DateSelect}
                placeholder="Select Date"
              // disabled={this.state.dropdown}
              >
                {filteredDate.map((obj: any) => (
                  <Option key={obj} value={obj}>
                    {obj}
                  </Option>
                ))}
              </Select>
            </div>
            <div
              style={{ float: "right", marginLeft: "auto" }}
            >
              <Select
                // mode="multiple"
                className="header-btn"
                showSearch
                // style={{ width: `${(8 * selectedApplication.length) + 500}px`, height: "100px", backgroundColor: "white" }}
                value={selectedApplication === '' ? (undefined) : (selectedApplication)}
                onChange={ApplicationSelect}
                placeholder="Select Application"
              // disabled={this.state.dropdown}
              >
                {filteredApplication.map((obj: any) => (
                  <Option key={obj} value={obj}>
                    {obj}
                  </Option>
                ))}
              </Select>
            </div>
            <div
              style={{ float: "right", marginLeft: "auto" }}
            >
              <Select
                // mode="multiple"
                className="header-btn"
                showSearch
                // style={{ width: `${(8 * selectedDomain.length) + 500}px`, height: "100px", backgroundColor: "white" }}
                value={selectedDomain === '' ? (undefined) : (selectedDomain)}
                onChange={DomainSelect}
                placeholder="Select Domain"
              // disabled={this.state.dropdown}
              >
                {filteredDomain.map((obj: any) => (
                  <Option key={obj} value={obj}>
                    {obj}
                  </Option>
                ))}
              </Select>
              {/* <span className='header-text'>Select Domain&nbsp;&nbsp;</span> */}
            </div>
            <div
              style={{ float: "right", marginLeft: "auto" }}
            >
              <Select
                className="header-btn"
                showSearch
                // style={{ width: `100px`, height: "100px", }}
                value={selectedType === '' ? (undefined) : (selectedType)}
                onChange={TypeSelect}
                placeholder="Select Type"
              // disabled={this.state.dropdown}
              >
                {props.uniqueType.length !== 0 ? (props.uniqueType.map((obj: any) => (
                  <Option key={obj} value={obj}>
                    {obj}
                  </Option>
                ))) : (null)}

              </Select>
              {/* <span className='header-text'>Type&nbsp;&nbsp;</span> */}
            </div>
          </Header>
          <Layout>
            <Layout className="site-layout">
              <Content style={{ overflow: 'hidden' }}>
                <Divider type="vertical" style={{ width: "50%" }}>
                  <div className="site-layout-background report-graph" style={{ padding: 80, textAlign: 'center' }}>
                    <div
                      className="content-div"
                    // style={{ padding: 24, minHeight: 360 }}
                    >
                      <h1>Tosca Report-Health check</h1>
                      <div className="chart-view" >
                        <PieChart data={PieChartFunctionFirstLevel(props.toscaReports, selectedType, selectedDomain, selectedApplication)} />
                      </div>
                    </div>
                  </div>
                </Divider>
                <Divider type="vertical" style={{ width: "45%" }}>
                  <div className="site-layout-background report-graph" style={{ padding: 80, textAlign: 'center' }}>
                    <div
                      className="content-div"
                    // style={{ padding: 24, minHeight: 360 }}
                    >
                      <h1>Execution Status</h1>
                      <div style={{ float: "right", marginLeft: "auto" }}>
                      <Button type="primary">
                        <CSVLink
                          filename={"ToscaReport.csv"}
                          data={TableDataFilter(props.toscaReports, selectedType, selectedDomain, selectedApplication, selectedDate)}
                          className="btn btn-primary"
                          onClick={() => {
                            message.success("The file is downloading")
                          }}
                        >
                          Export to CSV
                        </CSVLink>
                      </Button>
                      </div>
                      <div className="chart-view" >
                        <ToscaReportTable data={TableDataFilter(props.toscaReports, selectedType, selectedDomain, selectedApplication, selectedDate)} />
                      </div>
                    </div>
                  </div>
                </Divider>
              </Content>
              {/* <Footer className="footer-style">PETRA REPORT ©2021 Created by Expleo</Footer> */}
            </Layout>
          </Layout>
        </Layout>

      ) : (null)}
    </>

  );
}

const mapStateToProps = (state: any) => ({
  toscaReports: state.ToscaReducer.toscaReports,
  uniqueType: state.ToscaReducer.uniqueType,
  tableData: state.ToscaReducer.tableData,
});

export default connect(mapStateToProps, {
  fetchToscaReport, TableViewFunction
})(ToscaReport);
