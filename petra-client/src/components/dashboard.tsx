import { Component } from 'react';
import { connect } from "react-redux";
import { fetchReport,fetchUniqueProject,fetchUniqueRelease,SelectedProject } from "../redux/actions/reportAction";
import {fetchToscaReport} from "../redux/actions/toscaAction";
import ReportView from "./reportview";

/* Declaring Props variable */
type MyProps = { 
    fetchReport:any;
    fetchUniqueProject:any;
    fetchUniqueRelease:any;
    recentData:any;
    // allReports: any;
    SelectedProject:any;
    fetchToscaReport: any;
};
  
  /* Declaring State variable */
  type MyState = {
  };

class Dashboard extends Component<MyProps, MyState> {
    componentWillMount() {
        this.props.fetchReport();
        this.props.fetchToscaReport();
        this.props.fetchUniqueProject();
        this.props.fetchUniqueRelease(this.props.recentData.ProjectName);
        this.props.SelectedProject(this.props.recentData.ProjectName);
      }
    render() {
        return (
            <div>
                <ReportView></ReportView>
            </div>
        )
    }
}

const mapStateToProps = (state:any) => ({
    recentData: state.reportReducer.recentData
  });
  
  export default connect(mapStateToProps, {
    fetchReport,fetchUniqueProject,fetchUniqueRelease,SelectedProject,fetchToscaReport
  })(Dashboard);