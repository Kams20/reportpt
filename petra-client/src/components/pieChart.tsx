import React from 'react';
import Highcharts from "highcharts";
import Exporting from "highcharts/modules/exporting";
import NoData from "highcharts/modules/no-data-to-display";
import HighchartsReact from "highcharts-react-official";
import { connect } from "react-redux";
import { TableViewFunction } from "../redux/actions/toscaAction";
import {TableDataFilter } from "../chartFunctions/toscaReportFunction";


Exporting(Highcharts);
NoData(Highcharts);

let chartData: any;

const PieChart = (props:any) => {

  
  /* Test Execution - Chart Options*/
  const chartOptions = () =>{
      chartData =props.data;
    return {
        series: [{
            type: 'pie',
            data: chartData
        }],
      title: {
        text: 'Smoke Test'
      },
      legend: { enabled: true },
      xAxis: {
        categories: chartData.labels,
      },
      exporting: {
        enabled: false, // hide button
      },
      tooltip: {
        pointFormat: '{point.Type}: <b>{point.percentage:.1f}%</b> <br>{point.Domain}'
    },
      labels: {
        items: [{
          style: {
            left: '50px',
            top: '18px',
            color: 'black',
            textOverflow: 'none'
          }
        }]
      },
      yAxis: {
        title: {
          text: null,
        },
        gridLineWidth: 0,
      },
      
      plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.percentage:.1f} %'
            },
            // events: {
            //   click: (function (self:any) {
            //     return function (event:any) {
            //       console.log(event.point.options)
            //       let point = event.point.options,
            //       TData = TableDataFilter(props.toscaReports,point.Type,point.Domain,point.name);
            //       props.TableViewFunction(TData);
            //     };
            //   })(this),
            // },
        },
       
    },

      credits: {
        enabled: false,
      },
    };
  }

  return (
    <div>
      <HighchartsReact
        highcharts={Highcharts}
        options={chartOptions()}
      />
    </div>
  )

}
  

const mapStateToProps = (state: any) => ({
  toscaReports: state.ToscaReducer.toscaReports,
});

export default connect(mapStateToProps, {
  TableViewFunction 
})(PieChart);