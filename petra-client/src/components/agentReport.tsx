import React, { Component } from 'react';
import Highcharts from "highcharts";
import Exporting from "highcharts/modules/exporting";
import NoData from "highcharts/modules/no-data-to-display";
import HighchartsReact from "highcharts-react-official";
import { ByRelease, ByEXECUTEDON } from "../chartFunctions/reportchartFunctions";
import { connect } from "react-redux";
import { config } from '../config/config';
import { SelectedRelease } from "../redux/actions/reportAction";


Exporting(Highcharts);
NoData(Highcharts);

let chartData: any;

/* Declaring Props variable */
type MyProps = {
  allReports: any;
  LOB: any;
  chartType: any;
  selectedProject: any;
  releaseData: any;
  SelectedRelease: any;
  selectedTransaction: any;

};

/* Declaring State variable */
type MyState = {
  chartData: any;
};

class AgentReportCharts extends Component<MyProps, MyState> {
  constructor(props: any) {
    super(props);
    this.state = {
      chartData: {}
    };
  }

  selectedReleaseValidator(Data: any) {
    if (Data.length === 0) {
      return false;
    } else if (this.props.releaseData.length === Data.length) {
      return false;
    } else {
      return true;
    }
  }

  /* Test Execution - Chart Options*/
  chartOptions() {
    if (this.props.chartType === config.release) {
      chartData = ByRelease(this.props.allReports, this.props.LOB, this.props.selectedProject, this.props.releaseData, this.props.selectedTransaction);
      if (this.selectedReleaseValidator(chartData.selectedRelease)) {
        this.props.SelectedRelease(chartData.selectedRelease)
      }
    } else if (this.props.chartType === config.executedon) {
      chartData = ByEXECUTEDON(this.props.allReports, this.props.LOB);
    }

    return {
      series: chartData.datasets,
      title: {
        text: null
      },
      xAxis: {
        categories: chartData.labels,
      },
      exporting: {
        enabled: false, // hide button
      },
      tooltip: {
        outside: true,
      },
      labels: {
        items: [{
          style: {
            left: '50px',
            top: '18px',
            color: 'black',
            textOverflow: 'none'
          }
        }]
      },
      yAxis: {
        title: {
          text: null,
        },
        gridLineWidth: 0,
      },
      plotOptions: {
        series: {
          cursor: "pointer",
          borderWidth: 0,
          dataLabels: {
            enabled: false,
            // formatter: function () {
            //   return this.y !== 0 ? this.y : "";
            // },
            style: {
              textShadow: false,
              fontSize: "10px",
              color: "#000000",
              textOutline: false,
            },
          },
        },
      },

      credits: {
        enabled: false,
      },
    };
  }



  render() {
    return (
      <div>
        <HighchartsReact
          highcharts={Highcharts}
          options={this.chartOptions()}
        />
      </div>
    )
  }
}


const mapStateToProps = (state: any) => ({
  allReports: state.reportReducer.allreports,
  selectedProject: state.reportReducer.selectedProject,
  releaseData: state.reportReducer.selectedRelease,
  selectedTransaction: state.reportReducer.selectedTranscation
});

export default connect(mapStateToProps, {
  SelectedRelease
})(AgentReportCharts);