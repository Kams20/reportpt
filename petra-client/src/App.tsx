import React from "react";
import {BrowserRouter,Router, Route} from "react-router-dom";
import routes from "./routes";
import withTracker from "./withTracker";
import './App.css'
import {Provider} from 'react-redux';
import store from './store';
import {config} from './config/config';
import { createBrowserHistory } from 'history';

let history = createBrowserHistory();
// eslint-disable-next-line import/no-anonymous-default-export
export default() => (
    <Provider store={store}>
        <BrowserRouter basename={config.appBaseName || ""}>
          <Router history = {history}>
            <div>
                {routes.map((route, index) => {
                    return (
                        <Route
                            key={index}
                            path={route.path}
                            exact={route.exact}
                            component={withTracker((props:any) => {
                            return (
                                    <route.component {...props}/>
                            );
                        })}/>
                    );
                })}
            </div>
            </Router>
        </BrowserRouter>
    </Provider>
);
