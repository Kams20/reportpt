import fetch from "../../utils/leoFetch";
import { config } from "../../config/config";
import { FETCH_REPORT, FETCH_UNQ_PROJECTS, FETCH_UNQ_RELEASE, FETCH_UNQ_APPLICATION, SELECTED_PROJECT, RECENT_PROJECT, SELECTED_RELEASE, FETCH_UNQ_TRANSCATION, SELECTED_TRANSCATION } from "./types";

export const fetchReport = () => (dispatch: any, option: any) => {
  fetch(config.serviceUrl + config.allReports, option)
    .then((res) => res.json())
    .then((allReports) => {
      dispatch({
        payload: allReports.data,
        type: FETCH_REPORT,
      });
      dispatch({
        payload: allReports.recentProject,
        type: RECENT_PROJECT,
      });
    });
};

export const fetchUniqueProject = () => (dispatch: any, option: any) => {
  fetch(config.serviceUrl + config.uniqueProjects, option)
    .then((res) => res.json())
    .then((projects) => {
      dispatch({
        payload: projects.data,
        type: FETCH_UNQ_PROJECTS,
      });
    });
};

export const fetchUniqueRelease =
  (data: any) => (dispatch: any, option: any) => {
    fetch(config.serviceUrl + config.uniqueReleases + "/" + data, option)
      .then((res) => res.json())
      .then((releases) => {
        dispatch({
          payload: releases.data,
          type: FETCH_UNQ_RELEASE,
        });
        dispatch({
          payload: releases.applicationData,
          type: FETCH_UNQ_APPLICATION,
        });
        dispatch({
          payload: releases.TranscationData,
          type: FETCH_UNQ_TRANSCATION,
        });
      })
      .catch((err) => {
        dispatch({
          payload: [],
          type: FETCH_UNQ_APPLICATION,
        });
        dispatch({
          payload: [],
          type: FETCH_UNQ_RELEASE,
        });
        dispatch({
          payload: [],
          type: FETCH_UNQ_TRANSCATION,
        });
      });
  };

export const SelectedProject = (data: any) => (dispatch: any, option: any) => {
  dispatch({
    payload: data,
    type: SELECTED_PROJECT,
  });
};

export const SelectedRelease = (data: any) => (dispatch: any, option: any) => {
  dispatch({
    payload: data,
    type: SELECTED_RELEASE,
  });
};

export const SelectedTranscation = (data: any) => (dispatch: any, option: any) => {
  dispatch({
    payload: data,
    type: SELECTED_TRANSCATION,
  });
};
