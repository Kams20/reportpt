import fetch from "../../utils/leoFetch";
import { config } from "../../config/config";
import { FETCH_TOSCA_REPORT,UNIQUE_TYPE,TABLE_DATA} from "./types";

export const fetchToscaReport = () => (dispatch: any, option: any) => {
  fetch(config.serviceUrl + config.toscaReports, option)
    .then((res) => res.json())
    .then((allReports) => {
      let distnictType:any = [];
      if(allReports.data.length !== 0){
        distnictType = [...new Set(allReports.data.map((x:any )=> x.Type))];
      }
      dispatch({
        payload: allReports.data,
        type: FETCH_TOSCA_REPORT,
      });
      dispatch({
        payload: distnictType,
        type: UNIQUE_TYPE,
      });
    });
};


export const TableViewFunction = (Data:any) => (dispatch: any, option: any) => {
      dispatch({
        payload: Data,
        type: TABLE_DATA,
      });
};

