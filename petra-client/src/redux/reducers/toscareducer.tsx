import {
    FETCH_TOSCA_REPORT,
    UNIQUE_TYPE,
    TABLE_DATA
   } from "../actions/types";
   
   const initialState = {
     toscaReports: [],
     uniqueType: [],
     tableData:[]
    
   };

const ToscaReducer =  (state = initialState, action: any) => {
    switch (action.type) {
      case FETCH_TOSCA_REPORT:
        return {
          ...state,
          toscaReports: action.payload,
        };
        case UNIQUE_TYPE:
          return {
            ...state,
            uniqueType: action.payload,
          };
          case TABLE_DATA:
            return {
              ...state,
              tableData: action.payload,
            };
      default:
        return state;
    }
  }
  
  export default ToscaReducer;