import {combineReducers} from 'redux';
import reportReducer from  './reportReducer';
import ToscaReducer from './toscareducer'

export default combineReducers({
    reportReducer: reportReducer,
    ToscaReducer:ToscaReducer
});
