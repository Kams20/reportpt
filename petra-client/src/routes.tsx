import React from "react";
import { Redirect } from "react-router-dom";
import Dashboard from './components/dashboard';
import ToscaReport from "./components/toascaReport";

// eslint-disable-next-line import/no-anonymous-default-export
export default [
    {
        path: "/",
        exact: true,
        component: () => <Redirect to="/report"/>
      },
    {
        path: "/report",
        component: Dashboard
        },
        {
          path: "/toscaReport",
          component: ToscaReport
          }
];
