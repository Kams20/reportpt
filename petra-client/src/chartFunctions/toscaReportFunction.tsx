
import { config } from "../config/config";

/*By Executed on */
export const PieChartFunctionFirstLevel1 = (allreports: any, Type: string, Domain: string, Application: string) => {
  console.log(Type, Domain, Application)
  if (allreports !== undefined) {
    let FinalPtData = [];
    let data: any = [];
    FinalPtData = allreports;
    if (Type !== "") {
      FinalPtData = allreports.filter((data: any) => { return (data.Type === Type) });
    }
    if (Domain !== "") {
      FinalPtData = allreports.filter((data: any) => { return (data.Domain === Domain) });
    }
    if (Application !== "") {
      FinalPtData = allreports.filter((data: any) => { return (data.Application === Application) });
    }

    if (FinalPtData.length !== 0) {
      for (let i = 0; i < FinalPtData.length; i++) {
        let obj = {
          name: FinalPtData[i].Application,
          y: FinalPtData[i].TotaltestCase,
          Domain: FinalPtData[i].Domain,
          Type: FinalPtData[i].Type,
        }
        data = data.concat(obj)
      }
      return data;
    } else {
      let emppriority = { labels: [config.nodatadisplay] };
      return emppriority;
    }
  } else {
    let emppriority = { labels: [config.nodatadisplay] };
    return emppriority;
  }
};

/*By Executed on */
export const ExecutionStatus = (allreports: any) => {
  if (allreports !== undefined) {
    let data: any = [];
    if (Object.keys(allreports).length !== 0) {
      let obj = {
        name: "Passed",
        y: allreports.Passed
      }
      data = data.concat(obj);
      let obj1 = {
        name: "Failed",
        y: allreports.Failed
      }
      data = data.concat(obj1);

      return data;
    } else {
      let emppriority = { labels: [config.nodatadisplay] };
      return emppriority;
    }
  } else {
    let emppriority = { labels: [config.nodatadisplay] };
    return emppriority;
  }
};

/*Table data */

/*By Executed on */
export const TableDataFilter = (allreports: any, Type: string, Domain: string, Application: string,Date:string) => {
  console.log(Type, Domain, Application,Date)
  if (allreports !== undefined) {
    let FinalPtData = [];
    FinalPtData = allreports;
    if (Type !== "") {
      FinalPtData = allreports.filter((data: any) => { return (data.Type === Type) });
    }
    if (Domain !== "") {
      FinalPtData = FinalPtData.filter((data: any) => { return (data.Domain === Domain) });
    }
    if (Application !== "") {
      FinalPtData = FinalPtData.filter((data: any) => { return (data.Application === Application) });
    }
    if (Date !== "") {
      FinalPtData = FinalPtData.filter((data: any) => { return (data.Date === Date) });
    }

    return FinalPtData;
  } else {

    return [];
  }
};



/*By Executed on */
export const PieChartFunctionFirstLevel = (allreports: any, Type: string, Domain: string, Application: string) => {
  console.log(Type, Domain, Application)
  if (allreports !== undefined) {
    let FinalPtData = [];
    let sortingOrder: any = [];
    FinalPtData = allreports;
    if (Type !== "") {
      FinalPtData = allreports.filter((data: any) => { return (data.Type === Type) });
    }
    if (Domain !== "") {
      FinalPtData = FinalPtData.filter((data: any) => { return (data.Domain === Domain) });
    }
    if (Application !== "") {
      FinalPtData = FinalPtData.filter((data: any) => { return (data.Application === Application) });
    }

    FinalPtData.forEach((sort: any) => {
      sortingOrder.push(sort.Application);
    });
    if (sortingOrder.length !== 0) {
      sortingOrder = sortingOrder.filter(
        (value: any, index: any) => sortingOrder.indexOf(value) === index
      );
    }

    if (FinalPtData.length !== 0) {
      let series = [];
      let Application = [];
      for (let i = 0; i < FinalPtData.length; i++) {
        let toscaChartData = FinalPtData[i];
        if (Application.length === 0) {
          Application.push(toscaChartData.Application);
          let data = [];
          let drillobject = {
            name: toscaChartData.Application,
            y: toscaChartData.TotaltestCase,
            Domain: toscaChartData.Domain,
            Type: toscaChartData.Type,
          };
          data.push(drillobject);
          let seriesObj = {
            name: toscaChartData.Application,
            // pointWidth: 100,
            data: data,
          };
          series.push(seriesObj);
        } else {
          let ApplicationFind = Application.filter((col) => {
            return col === toscaChartData.Application;
          });
          if (ApplicationFind.length === 0) {
            Application.push(toscaChartData.Application);
            let data = [];
            let drillobject = {
              name: toscaChartData.Application,
              y: toscaChartData.TotaltestCase,
              Domain: toscaChartData.Domain,
              Type: toscaChartData.Type,
            };
            data.push(drillobject);
            let seriesObj = {
              name: toscaChartData.Application,
              // pointWidth: 100,
              data: data,
            };
            series.push(seriesObj);
          } else {
            let EditSerisObj = series.filter((col) => {
              return col.name === toscaChartData.Application;
            });
            let drillobject = {
              name: toscaChartData.Application,
              y: toscaChartData.TotaltestCase,
              Domain: toscaChartData.Domain,
              Type: toscaChartData.Type,
            };
            EditSerisObj[0].data.push(drillobject);
          }
        }
      }
      let Finalseries: any = [];
      series.forEach((s) => {
        s.data = addSeriesMultilevel(s.data, sortingOrder);
        Finalseries.push(s);
      });
      Finalseries = ArrayConstructionFunction(Finalseries);
      console.log(Finalseries)
      return Finalseries;
    } else {
      let emppriority = { labels: [config.nodatadisplay] };
      return emppriority;
    }
  } else {
    let emppriority = { labels: [config.nodatadisplay] };
    return emppriority;
  }
};


export const addSeriesMultilevel = (data: any, sortingOrder: any) => {
  let FinalData: any = [];
  if (data.length !== 0) {
    data.forEach((d: any) => {
      let sum = 0;
      if (FinalData.length === 0) {
        let FilterElement = data.filter((subdata: any) => {
          return d.name === subdata.name;
        });
        FilterElement.forEach((datasum: any) => {
          sum = sum + datasum.y;
        });
        let finalobj = {
          name: d.name,
          y: sum,
          Domain: d.Domain,
          Type: d.Type,
        };
        FinalData.push(finalobj);
      } else {
        let m = FinalData.filter((matchdata: any) => {
          return d.name === matchdata.name;
        });
        if (m.length === 0) {
          let FilterElement = data.filter((subdata: any) => {
            return d.name === subdata.name;
          });
          FilterElement.forEach((datasum: any) => {
            sum = sum + datasum.y;
          });
          let finalobj = {
            name: d.name,
            y: sum,
            Domain: d.Domain,
            Type: d.Type,
          };
          FinalData.push(finalobj);
        }
      }
    });
  }
  return Datastoring(FinalData, sortingOrder);
}


/*Sorting Function*/

export function Datastoring(data: any, sortOrder: any) {
  let FinalArray: any = [];
  for (let b = 0; b < sortOrder.length; b++) {
    let TempItem = data.filter((col: any) => {
      return col.name === sortOrder[b];
    });
    if (TempItem.length === 0) {
      let tempObj = {
        name: sortOrder[b],
        y: 0
      }
      FinalArray.splice(b, 0, tempObj);
      data.splice(b, 0, tempObj);
    }
    if (TempItem.length !== 0) {
      FinalArray.push(TempItem[0]);
    }
  }
  return FinalArray;
}

function ArrayConstructionFunction(data: any) {
  let finalData: any = [];
  data.forEach((d: any) => {
    let filterData = d.data.filter((col: any) => { return (col.name === d.name) });
    if (filterData.length !== 0) {
      let obj = {
        name: d.name,
        y: filterData[0].y,
        Domain: filterData[0].Domain,
        Type: filterData[0].Type,
      }
      finalData.push(obj);
    }
  });
  return finalData;

}