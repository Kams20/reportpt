import { config } from "../config/config";
import { releseValueforBaselineReport, releseValueforBaselineReportByExecutedon } from "./slaReport";
import { maxDateFinding } from "./maxDateFunctions";

export const ByRelease = (allreports: any, LOB: any, ProjectName: any, selectedRelease: any, selectedTransaction: any) => {
  let Bypriority = {};
  if (allreports !== undefined && ProjectName !== null) {
    let FinalPtData: any = [];
    let ptLabel = [];
    let releaseNamecollection = [];
    let data: any = [];
    FinalPtData = allreports.filter((col: any) => {
      return (
        col.CHANNEL === "Agent"
      );
    });;
    if (config.allLob !== LOB) {
      FinalPtData = FinalPtData.filter((col: any) => {
        return (
          col.LOB === LOB
        );
      });
    }
    if (ProjectName !== null) {
      FinalPtData = FinalPtData.filter((col: any) => {
        return (
          col.ProjectName === ProjectName
        );
      });
    }
    if (selectedTransaction.length !== 0) {
      FinalPtData = TransactionFilter(FinalPtData, selectedTransaction);
    }
    let maxdates = maxDateFinding(FinalPtData);
    FinalPtData = DateFilters(FinalPtData, maxdates, selectedRelease);
    if (FinalPtData.length !== 0) {
      for (let i = 0; i < FinalPtData.length; i++) {
        let countData: any = [];
        let ptData = FinalPtData[i];
        if (ptLabel.length === 0) {
          ptLabel.push(ptData.TRANSACTIONNAME);
          releaseNamecollection.push(ptData.Release + "_" + ptData.EXECUTEDON);
          countData.push(ptData.ResponseTimeInsecs);
          let dataObj = {
            type: 'column',
            name: ptData.Release + "_" + ptData.EXECUTEDON,
            data: countData,
          };
          data.push(dataObj);
        } else {
          let statusFind = ptLabel.filter((col) => {
            return col === ptData.TRANSACTIONNAME;
          });
          if (statusFind.length === 0) {
            ptLabel.push(ptData.TRANSACTIONNAME);
            let index = ptLabel.length - 1;
            let priorityFind = releaseNamecollection.filter((col) => {
              return col === ptData.Release + "_" + ptData.EXECUTEDON;
            });
            if (priorityFind.length === 0) {
              releaseNamecollection.push(ptData.Release + "_" + ptData.EXECUTEDON);
              for (let j = 0; j <= index; j++) {
                if (j === index) {
                  countData.splice(j, 1, ptData.ResponseTimeInsecs);
                } else {
                  countData.splice(j, 1, 0);
                }
              }
              let dataObj = {
                type: 'column',
                name: ptData.Release + "_" + ptData.EXECUTEDON,
                data: countData,
              };
              data.push(dataObj);
            } else {
              for (let s = 0; s < data.length; s++) {
                let d = data[s];
                if (d.name === ptData.Release + "_" + ptData.EXECUTEDON) {
                  for (let k = 0; k <= index; k++) {
                    if (k === index) {
                      d.data.splice(index, 1,ptData.ResponseTimeInsecs);
                    } else {
                      if (
                        d.data[k] === 0 ||
                        d.data[k] === null ||
                        d.data[k] === undefined
                      ) {
                        d.data.splice(k, 1, 0);
                      }
                    }
                  }
                }
              }
            }
          } else {
            let index = ptLabel.findIndex(
              (img) => img === ptData.TRANSACTIONNAME
            );
            let priorityFind = releaseNamecollection.filter((col) => {
              return col === ptData.Release + "_" + ptData.EXECUTEDON;
            });
            if (priorityFind.length === 0) {
              releaseNamecollection.push(ptData.Release + "_" + ptData.EXECUTEDON);
              for (let j = 0; j <= index; j++) {
                if (j === index) {
                  countData.splice(j, 1,ptData.ResponseTimeInsecs);
                } else {
                  countData.splice(j, 1, 0);
                }
              }
              let dataObj = {
                type: 'column',
                name: ptData.Release + "_" + ptData.EXECUTEDON,
                data: countData,
              };
              data.push(dataObj);
            } else {
              for (let s = 0; s < data.length; s++) {
                let d = data[s];
                if (d.name === ptData.Release + "_" + ptData.EXECUTEDON) {
                  for (let k = 0; k <= index; k++) {
                    if (k === index) {
                      let addCount = d.data[k] + ptData.ResponseTimeInsecs;
                      if (isNaN(addCount)) {
                        addCount = ptData.ResponseTimeInsecs;
                      }
                      d.data.splice(index, 1, addCount);
                    } else {
                      if (
                        d.data[k] === 0 ||
                        d.data[k] === null ||
                        d.data[k] === undefined
                      ) {
                        d.data.splice(k, 1, 0);
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
      let sla: any = releseValueforBaselineReport(FinalPtData, LOB);
      let formattedSLA: any = SLAFormatter(ptLabel, sla.datasets);
      let FindingSelectedRelease = ReleaseData(data, selectedRelease, selectedTransaction);
      data = data.concat(formattedSLA);
      Bypriority = {
        labels: ptLabel,
        datasets: data,
        selectedRelease: FindingSelectedRelease
      };
      console.log("Final Data >>>>>",Bypriority)
      return Bypriority;
    } else {
      return {
        labels: [],
        datasets: [],
        selectedRelease: []
      };
    }
  } else {
    return {
      labels: [],
      datasets: [],
      selectedRelease: []
    };
  }
};

/*By Executed on */
export const ByEXECUTEDON = (allreports: any, LOB: any) => {
  let Bypriority = {};
  if (allreports !== undefined) {
    let FinalPtData = [];
    let ptLabel = [];
    let EXECUTEDONNamecollection = [];
    let data: any = [];
    if (config.allLob !== LOB) {
      FinalPtData = allreports.filter((col: any) => {
        return (
          col.LOB === LOB
        );
      });
    } else {
      FinalPtData = allreports;
    }
    if (FinalPtData.length !== 0) {
      for (let i = 0; i < FinalPtData.length; i++) {
        let countData: any = [];
        let ptData = FinalPtData[i];
        if (ptLabel.length === 0) {
          ptLabel.push(ptData.EXECUTEDON);
          EXECUTEDONNamecollection.push(ptData.TRANSACTIONNAME);
          countData.push(1);
          let dataObj = {
            type: 'column',
            name: ptData.TRANSACTIONNAME,
            data: countData,
          };
          data.push(dataObj);
        } else {
          let statusFind = ptLabel.filter((col) => {
            return col === ptData.EXECUTEDON;
          });
          if (statusFind.length === 0) {
            ptLabel.push(ptData.EXECUTEDON);
            let index = ptLabel.length - 1;
            let priorityFind = EXECUTEDONNamecollection.filter((col) => {
              return col === ptData.TRANSACTIONNAME;
            });
            if (priorityFind.length === 0) {
              EXECUTEDONNamecollection.push(ptData.TRANSACTIONNAME);
              for (let j = 0; j <= index; j++) {
                if (j === index) {
                  countData.splice(j, 1, 1);
                } else {
                  countData.splice(j, 1, 0);
                }
              }
              let dataObj = {
                type: 'column',
                name: ptData.TRANSACTIONNAME,
                data: countData,
              };
              data.push(dataObj);
            } else {
              for (let s = 0; s < data.length; s++) {
                let d = data[s];
                if (d.name === ptData.TRANSACTIONNAME) {
                  for (let k = 0; k <= index; k++) {
                    if (k === index) {
                      d.data.splice(index, 1, 1);
                    } else {
                      if (
                        d.data[k] === 0 ||
                        d.data[k] === null ||
                        d.data[k] === undefined
                      ) {
                        d.data.splice(k, 1, 0);
                      }
                    }
                  }
                }
              }
            }
          } else {
            let index = ptLabel.findIndex(
              (img) => img === ptData.EXECUTEDON
            );
            let priorityFind = EXECUTEDONNamecollection.filter((col) => {
              return col === ptData.TRANSACTIONNAME;
            });
            if (priorityFind.length === 0) {
              EXECUTEDONNamecollection.push(ptData.TRANSACTIONNAME);
              for (let j = 0; j <= index; j++) {
                if (j === index) {
                  countData.splice(j, 1, 1);
                } else {
                  countData.splice(j, 1, 0);
                }
              }
              let dataObj = {
                type: 'column',
                name: ptData.TRANSACTIONNAME,
                data: countData,
              };
              data.push(dataObj);
            } else {
              for (let s = 0; s < data.length; s++) {
                let d = data[s];
                if (d.name === ptData.TRANSACTIONNAME) {
                  for (let k = 0; k <= index; k++) {
                    if (k === index) {
                      let addCount = d.data[k] + 1;
                      if (isNaN(addCount)) {
                        addCount = 1;
                      }
                      d.data.splice(index, 1, addCount);
                    } else {
                      if (
                        d.data[k] === 0 ||
                        d.data[k] === null ||
                        d.data[k] === undefined
                      ) {
                        d.data.splice(k, 1, 0);
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
      let sla: any = releseValueforBaselineReportByExecutedon(allreports, LOB);
      let formattedSLA: any = SLAFormatter(ptLabel, sla.datasets);
      data = data.concat(formattedSLA);
      Bypriority = {
        labels: ptLabel,
        datasets: data,
      };
      return Bypriority;
    } else {
      let emppriority = { labels: [config.nodatadisplay] };
      return emppriority;
    }
  } else {
    let emppriority = { labels: [config.nodatadisplay] };
    return emppriority;
  }
};


export const ReleaseBYBroker = (allreports: any, LOB: any, ProjectName: any, selectedRelease: any, selectedTransaction: any) => {
  let Bypriority = {};
  if (allreports !== undefined && ProjectName !== null) {
    let FinalPtData: any = [];
    let ptLabel = [];
    let releaseNamecollection = [];
    let data: any = [];
    FinalPtData = allreports.filter((col: any) => {
      return (
        col.CHANNEL === "Broker"
      );
    });;
    if (config.allLob !== LOB) {
      FinalPtData = FinalPtData.filter((col: any) => {
        return (
          col.LOB === LOB
        );
      });
    }
    if (ProjectName !== null) {
      FinalPtData = FinalPtData.filter((col: any) => {
        return (
          col.ProjectName === ProjectName
        );
      });
    }
    if (selectedTransaction.length !== 0) {
      FinalPtData = TransactionFilter(FinalPtData, selectedTransaction);
    }
    let maxdates = maxDateFinding(FinalPtData);
    FinalPtData = DateFilters(FinalPtData, maxdates, selectedRelease);
    if (FinalPtData.length !== 0) {
      for (let i = 0; i < FinalPtData.length; i++) {
        let countData: any = [];
        let ptData = FinalPtData[i];
        if (ptLabel.length === 0) {
          ptLabel.push(ptData.TRANSACTIONNAME);
          releaseNamecollection.push(ptData.Release + "_" + ptData.EXECUTEDON);
          countData.push(ptData.ResponseTimeInsecs);
          let dataObj = {
            type: 'column',
            name: ptData.Release + "_" + ptData.EXECUTEDON,
            data: countData,
          };
          data.push(dataObj);
        } else {
          let statusFind = ptLabel.filter((col) => {
            return col === ptData.TRANSACTIONNAME;
          });
          if (statusFind.length === 0) {
            ptLabel.push(ptData.TRANSACTIONNAME);
            let index = ptLabel.length - 1;
            let priorityFind = releaseNamecollection.filter((col) => {
              return col === ptData.Release + "_" + ptData.EXECUTEDON;
            });
            if (priorityFind.length === 0) {
              releaseNamecollection.push(ptData.Release + "_" + ptData.EXECUTEDON);
              for (let j = 0; j <= index; j++) {
                if (j === index) {
                  countData.splice(j, 1, ptData.ResponseTimeInsecs);
                } else {
                  countData.splice(j, 1, 0);
                }
              }
              let dataObj = {
                type: 'column',
                name: ptData.Release + "_" + ptData.EXECUTEDON,
                data: countData,
              };
              data.push(dataObj);
            } else {
              for (let s = 0; s < data.length; s++) {
                let d = data[s];
                if (d.name === ptData.Release + "_" + ptData.EXECUTEDON) {
                  for (let k = 0; k <= index; k++) {
                    if (k === index) {
                      d.data.splice(index, 1,ptData.ResponseTimeInsecs);
                    } else {
                      if (
                        d.data[k] === 0 ||
                        d.data[k] === null ||
                        d.data[k] === undefined
                      ) {
                        d.data.splice(k, 1, 0);
                      }
                    }
                  }
                }
              }
            }
          } else {
            let index = ptLabel.findIndex(
              (img) => img === ptData.TRANSACTIONNAME
            );
            let priorityFind = releaseNamecollection.filter((col) => {
              return col === ptData.Release + "_" + ptData.EXECUTEDON;
            });
            if (priorityFind.length === 0) {
              releaseNamecollection.push(ptData.Release + "_" + ptData.EXECUTEDON);
              for (let j = 0; j <= index; j++) {
                if (j === index) {
                  countData.splice(j, 1,ptData.ResponseTimeInsecs);
                } else {
                  countData.splice(j, 1, 0);
                }
              }
              let dataObj = {
                type: 'column',
                name: ptData.Release + "_" + ptData.EXECUTEDON,
                data: countData,
              };
              data.push(dataObj);
            } else {
              for (let s = 0; s < data.length; s++) {
                let d = data[s];
                if (d.name === ptData.Release + "_" + ptData.EXECUTEDON) {
                  for (let k = 0; k <= index; k++) {
                    if (k === index) {
                      let addCount = d.data[k] + ptData.ResponseTimeInsecs;
                      if (isNaN(addCount)) {
                        addCount = ptData.ResponseTimeInsecs;
                      }
                      d.data.splice(index, 1, addCount);
                    } else {
                      if (
                        d.data[k] === 0 ||
                        d.data[k] === null ||
                        d.data[k] === undefined
                      ) {
                        d.data.splice(k, 1, 0);
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
      let sla: any = releseValueforBaselineReport(FinalPtData, LOB);
      let formattedSLA: any = SLAFormatter(ptLabel, sla.datasets);
      let FindingSelectedRelease = ReleaseData(data, selectedRelease, selectedTransaction);
      data = data.concat(formattedSLA);
      Bypriority = {
        labels: ptLabel,
        datasets: data,
        selectedRelease: FindingSelectedRelease
      };
      return Bypriority;
    } else {
      return {
        labels: [],
        datasets: [],
        selectedRelease: []
      };
    }
  } else {
    return {
      labels: [],
      datasets: [],
      selectedRelease: []
    };
  }
};

/* SLA Formatting Function */

function SLAFormatter(Labels: any, rawSLA: any) {
  console.log(rawSLA)
  let dataitem: any = [];
  for (let i = 0; i < Labels.length; i++) {
    let label = Labels[i];
    let findingmatch: any = rawSLA.filter((col: any) => {
      return (
        col.name === label
      );
    });
    if (findingmatch.length !== 0) {
      dataitem = dataitem.concat(Number(findingmatch[0].data[0]));
    }
  }
  let dataobj = {
    type: 'spline',
    name: 'Release Value For Baseline',
    data: DataitemLabelAddition(dataitem)
  };
  return dataobj;
}

function DateFilters(data: any, maxDate: any, selectedRelease: any) {
  let FinalResult: any = [];
  if (selectedRelease.length === 0) {
    maxDate.forEach((i: any) => {
      let filtered = data.filter((col: any) => {
        return (
          col.EXECUTEDON === i
        );
      });
      FinalResult = [...FinalResult, ...filtered]
    });
  } else {
    selectedRelease.forEach((i: any) => {
      let spliting = i.split("_");
      let filtered = data.filter((col: any) => {
        return (
          col.EXECUTEDON === spliting[1] &&
          col.Release === spliting[0]
        );
      });
      FinalResult = [...FinalResult, ...filtered]
    });
  }
  return FinalResult;
}

function DataitemLabelAddition(dataArray: any) {
  let FinalDataitem: any = [];
  dataArray.forEach((data: any) => {
    let newObj = {
      y: data,
      dataLabels: {
        enabled: true,
        formatter: function (this: any) {
          return this.y !== 0 ? this.y : " ";
        }
      }
    }
    FinalDataitem.push(newObj);
  });
  return FinalDataitem;
}

function ReleaseData(data: any, selectedRelease: any, selectedTransaction: any) {
  if (selectedTransaction.length === 0) {
    if (selectedRelease.length === 0) {
      let SelectedRelease: any = [];
      data.forEach((d: any) => {
        SelectedRelease.push(d.name);
      })
      SelectedRelease = SelectedRelease.filter(
        (value: any, index: any) => SelectedRelease.indexOf(value) === index
      );
      return SelectedRelease;

    }else{
      return selectedRelease;
    }
 
  } else {
    return selectedRelease;
  }
}

function TransactionFilter(Report: any, selectedTranscation: any) {
  let FinalResult: any = [];
  selectedTranscation.forEach((trans: any) => {
    let filtered = Report.filter((col: any) => {
      return (
        col.TRANSACTIONNAME === trans
      );
    });
    FinalResult = [...FinalResult, ...filtered]
  });
  return FinalResult;
}