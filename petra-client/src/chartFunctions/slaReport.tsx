import { config } from "../config/config";
// import {  maxDateFinding } from "./maxDateFunctions";


export const releseValueforBaselineReport = (allreports: any, LOB: any) => {
  let Bypriority = {};
  if (allreports !== undefined) {
    let FinalPtData = [];
    let ptLabel = [];
    let releaseNamecollection = [];
    let data = [];
    FinalPtData = allreports;
    if (config.allLob !== LOB) {
      FinalPtData = allreports.filter((col: any) => {
        return (
          col.LOB === LOB
        );
      });
    }
    // let maxdates = maxDateFinding(FinalPtData);
    // FinalPtData = DateFilters(FinalPtData,maxdates);
    if (FinalPtData.length !== 0) {
      for (let i = 0; i < FinalPtData.length; i++) {
        let countData: any = [];
        let ptData = FinalPtData[i];
        if (ptLabel.length === 0) {
          ptLabel.push(ptData.Release + "_" + ptData.EXECUTEDON);
          releaseNamecollection.push(ptData.TRANSACTIONNAME);
          countData.push(ptData.releseValueforBaseline);
          let dataObj = {
            type: 'spline',
            name: ptData.TRANSACTIONNAME,
            data: countData,
          };
          data.push(dataObj);
        } else {
          let statusFind = ptLabel.filter((col) => {
            return col === ptData.Release + "_" + ptData.EXECUTEDON;
          });
          if (statusFind.length === 0) {
            ptLabel.push(ptData.Release + "_" + ptData.EXECUTEDON);
            let index = ptLabel.length - 1;
            let priorityFind = releaseNamecollection.filter((col) => {
              return col === ptData.TRANSACTIONNAME;
            });
            if (priorityFind.length === 0) {
              releaseNamecollection.push(ptData.TRANSACTIONNAME);
              for (let j = 0; j <= index; j++) {
                if (j === index) {
                  countData.splice(j, 1, ptData.releseValueforBaseline);
                } else {
                  countData.splice(j, 1, ptData.releseValueforBaseline);
                }
              }
              let dataObj = {
                type: 'spline',
                name: ptData.TRANSACTIONNAME,
                data: countData,
              };
              data.push(dataObj);
            } else {
              for (let s = 0; s < data.length; s++) {
                let d = data[s];
                if (d.name === ptData.Release + "_" + ptData.EXECUTEDON) {
                  for (let k = 0; k <= index; k++) {
                    if (k === index) {
                      d.data.splice(index, 1, 1);
                    } else {
                      if (
                        d.data[k] === 0 ||
                        d.data[k] === null ||
                        d.data[k] === undefined
                      ) {
                        d.data.splice(k, 1, 0);
                      }
                    }
                  }
                }
              }
            }
          } else {
            let index = ptLabel.findIndex(
              (img) => img === ptData.Release + "_" + ptData.EXECUTEDON
            );
            let priorityFind = releaseNamecollection.filter((col) => {
              return col === ptData.TRANSACTIONNAME;
            });
            if (priorityFind.length === 0) {
              releaseNamecollection.push(ptData.TRANSACTIONNAME);
              for (let j = 0; j <= index; j++) {
                if (j === index) {
                  countData.splice(j, 1, ptData.releseValueforBaseline);
                } else {
                  countData.splice(j, 1, ptData.releseValueforBaseline);
                }
              }
              let dataObj = {
                type: 'spline',
                name: ptData.TRANSACTIONNAME,
                data: countData,
              };
              data.push(dataObj);
            } else {
              for (let s = 0; s < data.length; s++) {
                let d = data[s];
                if (d.name === ptData.Release + "_" + ptData.EXECUTEDON) {
                  for (let k = 0; k <= index; k++) {
                    if (k === index) {
                      let addCount;
                      if (ptData.releseValueforBaseline > d.data[k]) {
                        addCount = ptData.releseValueforBaseline;
                      } else {
                        addCount = d.data[k];
                      }
                      if (isNaN(addCount)) {
                        addCount = 0;
                      }
                      d.data.splice(index, 1, addCount);
                    } else {
                      if (
                        d.data[k] === 0 ||
                        d.data[k] === null ||
                        d.data[k] === undefined
                      ) {
                        d.data.splice(k, 1, 0);
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
      Bypriority = {
        labels: ptLabel,
        datasets: data,
      };
      return Bypriority;
    } else {
      return 0;
    }
  } else {
    return 0;
  }
};

/*By Executed on */
export const releseValueforBaselineReportByExecutedon = (allreports: any, LOB: any) => {
  let Bypriority = {};
  if (allreports !== undefined) {
    let FinalPtData = [];
    let ptLabel = [];
    let EXECUTEDONNamecollection = [];
    let data = [];
    if (config.allLob !== LOB) {
      FinalPtData = allreports.filter((col: any) => {
        return (
          col.LOB === LOB
        );
      });
    } else {
      FinalPtData = allreports;
    }
    if (FinalPtData.length !== 0) {
      for (let i = 0; i < FinalPtData.length; i++) {
        let countData: any = [];
        let ptData = FinalPtData[i];
        if (ptLabel.length === 0) {
          ptLabel.push(ptData.TRANSACTIONNAME);
          EXECUTEDONNamecollection.push(ptData.EXECUTEDON);
          countData.push(ptData.releseValueforBaseline);
          let dataObj = {
            type: 'spline',
            name: ptData.EXECUTEDON,
            data: countData,
          };
          data.push(dataObj);
        } else {
          let statusFind = ptLabel.filter((col) => {
            return col === ptData.TRANSACTIONNAME;
          });
          if (statusFind.length === 0) {
            ptLabel.push(ptData.TRANSACTIONNAME);
            let index = ptLabel.length - 1;
            let priorityFind = EXECUTEDONNamecollection.filter((col) => {
              return col === ptData.EXECUTEDON;
            });
            if (priorityFind.length === 0) {
              EXECUTEDONNamecollection.push(ptData.EXECUTEDON);
              for (let j = 0; j <= index; j++) {
                if (j === index) {
                  countData.splice(j, 1, ptData.releseValueforBaseline);
                } else {
                  countData.splice(j, 1, 0);
                }
              }
              let dataObj = {
                type: 'column',
                name: ptData.EXECUTEDON,
                data: countData,
              };
              data.push(dataObj);
            } else {
              for (let s = 0; s < data.length; s++) {
                let d = data[s];
                if (d.name === ptData.EXECUTEDON) {
                  for (let k = 0; k <= index; k++) {
                    if (k === index) {
                      d.data.splice(index, 1, 1);
                    } else {
                      if (
                        d.data[k] === 0 ||
                        d.data[k] === null ||
                        d.data[k] === undefined
                      ) {
                        d.data.splice(k, 1, 0);
                      }
                    }
                  }
                }
              }
            }
          } else {
            let index = ptLabel.findIndex(
              (img) => img === ptData.TRANSACTIONNAME
            );
            let priorityFind = EXECUTEDONNamecollection.filter((col) => {
              return col === ptData.EXECUTEDON;
            });
            if (priorityFind.length === 0) {
              EXECUTEDONNamecollection.push(ptData.EXECUTEDON);
              for (let j = 0; j <= index; j++) {
                if (j === index) {
                  countData.splice(j, 1, ptData.releseValueforBaseline);
                } else {
                  countData.splice(j, 1, 0);
                }
              }
              let dataObj = {
                type: 'column',
                name: ptData.EXECUTEDON,
                data: countData,
              };
              data.push(dataObj);
            } else {
              for (let s = 0; s < data.length; s++) {
                let d = data[s];
                if (d.name === ptData.EXECUTEDON) {
                  for (let k = 0; k <= index; k++) {
                    if (k === index) {
                      let addCount;
                      if (ptData.releseValueforBaseline > d.data[k]) {
                        addCount = ptData.releseValueforBaseline;
                      } else {
                        addCount = d.data[k];
                      }
                      if (isNaN(addCount)) {
                        addCount = 0;
                      }
                      d.data.splice(index, 1, addCount);
                    } else {
                      if (
                        d.data[k] === 0 ||
                        d.data[k] === null ||
                        d.data[k] === undefined
                      ) {
                        d.data.splice(k, 1, 0);
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
      Bypriority = {
        labels: ptLabel,
        datasets: data,
      };
      return Bypriority;
    } else {
      let emppriority = { labels: [config.nodatadisplay] };
      return emppriority;
    }
  } else {
    let emppriority = { labels: [config.nodatadisplay] };
    return emppriority;
  }
};

  // function DateFilters(data:any,maxDate: any){
  //   let FinalResult:any = [];
  //   maxDate.forEach((i:any) => {
  //     let filtered = data.filter((col:any) => {
  //       return (
  //         col.EXECUTEDON === i
  //       );
  //     });
  //     FinalResult = [...FinalResult,...filtered]
  //   });
  //   return FinalResult;
  // }